################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
acc_driver.obj: ../acc_driver.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --opt_for_speed=3 --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/inc" --include_path="C:/Users/Jaume/Documents/se_prac/LcdDriver" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/grlib" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/fonts" --include_path="C:/Users/Jaume/Documents/se_prac/sensors" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/cortex-m4" --include_path="C:/Users/Jaume/Documents/se_prac/ti/msp432" --include_path="C:/Users/Jaume/Documents/se_prac" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="acc_driver.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

adc14_driver.obj: ../adc14_driver.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --opt_for_speed=3 --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/inc" --include_path="C:/Users/Jaume/Documents/se_prac/LcdDriver" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/grlib" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/fonts" --include_path="C:/Users/Jaume/Documents/se_prac/sensors" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/cortex-m4" --include_path="C:/Users/Jaume/Documents/se_prac/ti/msp432" --include_path="C:/Users/Jaume/Documents/se_prac" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="adc14_driver.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

i2c_driver.obj: ../i2c_driver.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --opt_for_speed=3 --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/inc" --include_path="C:/Users/Jaume/Documents/se_prac/LcdDriver" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/grlib" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/fonts" --include_path="C:/Users/Jaume/Documents/se_prac/sensors" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/cortex-m4" --include_path="C:/Users/Jaume/Documents/se_prac/ti/msp432" --include_path="C:/Users/Jaume/Documents/se_prac" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="i2c_driver.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

main.obj: ../main.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --opt_for_speed=3 --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/inc" --include_path="C:/Users/Jaume/Documents/se_prac/LcdDriver" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/grlib" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/fonts" --include_path="C:/Users/Jaume/Documents/se_prac/sensors" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/cortex-m4" --include_path="C:/Users/Jaume/Documents/se_prac/ti/msp432" --include_path="C:/Users/Jaume/Documents/se_prac" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="main.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

msp432_startup_ccs.obj: ../msp432_startup_ccs.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --opt_for_speed=3 --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/inc" --include_path="C:/Users/Jaume/Documents/se_prac/LcdDriver" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/grlib" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/fonts" --include_path="C:/Users/Jaume/Documents/se_prac/sensors" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/cortex-m4" --include_path="C:/Users/Jaume/Documents/se_prac/ti/msp432" --include_path="C:/Users/Jaume/Documents/se_prac" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="msp432_startup_ccs.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

system_msp432p401r.obj: ../system_msp432p401r.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --opt_for_speed=3 --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/inc" --include_path="C:/Users/Jaume/Documents/se_prac/LcdDriver" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/grlib" --include_path="C:/Users/Jaume/Documents/se_prac/GrLib/fonts" --include_path="C:/Users/Jaume/Documents/se_prac/sensors" --include_path="C:/Users/Jaume/Documents/se_prac/freertos/cortex-m4" --include_path="C:/Users/Jaume/Documents/se_prac/ti/msp432" --include_path="C:/Users/Jaume/Documents/se_prac" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="system_msp432p401r.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


