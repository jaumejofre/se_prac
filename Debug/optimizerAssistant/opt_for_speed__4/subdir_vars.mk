################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432p401r.cmd 

C_SRCS += \
../acc_driver.c \
../adc14_driver.c \
../i2c_driver.c \
../main.c \
../msp432_startup_ccs.c \
../system_msp432p401r.c 

C_DEPS += \
./acc_driver.d \
./adc14_driver.d \
./i2c_driver.d \
./main.d \
./msp432_startup_ccs.d \
./system_msp432p401r.d 

OBJS += \
./acc_driver.obj \
./adc14_driver.obj \
./i2c_driver.obj \
./main.obj \
./msp432_startup_ccs.obj \
./system_msp432p401r.obj 

OBJS__QUOTED += \
"acc_driver.obj" \
"adc14_driver.obj" \
"i2c_driver.obj" \
"main.obj" \
"msp432_startup_ccs.obj" \
"system_msp432p401r.obj" 

C_DEPS__QUOTED += \
"acc_driver.d" \
"adc14_driver.d" \
"i2c_driver.d" \
"main.d" \
"msp432_startup_ccs.d" \
"system_msp432p401r.d" 

C_SRCS__QUOTED += \
"../acc_driver.c" \
"../adc14_driver.c" \
"../i2c_driver.c" \
"../main.c" \
"../msp432_startup_ccs.c" \
"../system_msp432p401r.c" 


