/*
 *
 * Jaume Jofre Bravo
 * Sistems Encastats Pr�ctica
 * UOC I semestre 2017-18
 *
 */

// Includes del driver
#include <adc14_driver.h>
#include "driverlib.h"

// Includes standard
#include <string.h>

// Includes FreeRTOS
#include "FreeRTOS.h"
#include "semphr.h"

//Nombre de canals del ADC
uint16_t resultsBuffer[NUM_ADC_CHANNELS];
extern SemaphoreHandle_t xBinarySemaphoreISR;

//Inicialitzaci� conversor
void init_ADC(void){

    ADC_reading_available = 0;

    /* Zero-filling buffer */
    memset(resultsBuffer, 0x00, NUM_ADC_CHANNELS);

    /* Setting reference voltage to 2.5  and enabling reference */
    MAP_REF_A_setReferenceVoltage(REF_A_VREF2_5V);
    MAP_REF_A_enableReferenceVoltage();

    /* Initializing ADC (MCLK/1/1) */
    MAP_ADC14_enableModule();
    MAP_ADC14_initModule(ADC_CLOCKSOURCE_MCLK, ADC_PREDIVIDER_1, ADC_DIVIDER_1, 0);

    /* Configuring GPIOs for Analog In */
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P6, GPIO_PIN1, GPIO_TERTIARY_MODULE_FUNCTION);
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P4, GPIO_PIN0 | GPIO_PIN2, GPIO_TERTIARY_MODULE_FUNCTION);


    /* Configuring ADC Memory (ADC_MEM0 - ADC_MEM2 (A14, A13, A11)  with no repeat)
     * with internal 3.3v reference */
    MAP_ADC14_configureMultiSequenceMode(ADC_MEM0, ADC_MEM2, false);
    MAP_ADC14_configureConversionMemory(ADC_MEM0, ADC_VREFPOS_AVCC_VREFNEG_VSS, ADC_INPUT_A14, false);
    MAP_ADC14_configureConversionMemory(ADC_MEM1, ADC_VREFPOS_AVCC_VREFNEG_VSS, ADC_INPUT_A13, false);
    MAP_ADC14_configureConversionMemory(ADC_MEM2, ADC_VREFPOS_AVCC_VREFNEG_VSS, ADC_INPUT_A11, false);

    //Cal afegir aquesta l�nia perque sino la interrupci� ADC14 no entra mai
    MAP_Interrupt_setPriority(INT_ADC14, 0xA0);
    /* Enabling the interrupt when a conversion on channel 2 (end of sequence)
     *  is complete and enabling conversions */
    // Corresponde a ADC_MEM2
    MAP_ADC14_enableInterrupt(ADC_INT2);

    /* Enabling Interrupts */
    MAP_Interrupt_enableInterrupt(INT_ADC14);
    //MAP_Interrupt_enableMaster();

    /* Setting up the sample timer to automatically step through the sequence
     * convert.
     */
    MAP_ADC14_enableSampleTimer(ADC_AUTOMATIC_ITERATION);
}

//Recupera lectures ADC i les deixa al buffer
uint16_t *ADC_read(void){

    /* Triggering the start of the sample */
    MAP_ADC14_enableConversion();
    MAP_ADC14_toggleConversionTrigger();
    if( xSemaphoreTake( xBinarySemaphoreISR, portMAX_DELAY ) == pdPASS );
    return resultsBuffer;
}

//Interrupci� ADC que activa sem�for
void ADC14_IRQHandler(void)
{
    uint64_t status;
    static BaseType_t xHigherPriorityTaskWoken;

    status = MAP_ADC14_getEnabledInterruptStatus();
    MAP_ADC14_clearInterruptFlag(status);

    if (status & ADC_INT2)
    {
        MAP_ADC14_getMultiSequenceResult(resultsBuffer);

        //Llibera el semafor
        xHigherPriorityTaskWoken = pdFALSE;
        xSemaphoreGiveFromISR(xBinarySemaphoreISR, &xHigherPriorityTaskWoken);
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}
