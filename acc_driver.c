/*
 * acc_driver.c
 *
 * Jaume Jofre Bravo
 * Sistems Encastats Pr�ctica
 * UOC I semestre 2017-18
 *
 */

// Includes standard
#include <acc_driver.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "adc14_driver.h"

/* Reverses a string 'str' of length 'len' */
void reverse(char *str, int len)
{
    int i=0, j=len-1, temp;
    while (i<j)

    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
}

/* Converts a given integer x to string str[].  d is the number
  of digits required in output. If d is more than the number
  of digits in x, then 0s are added at the beginning */
int intToStr(int x, char str[], int d)
{
    int i = 0;
    while (x)
    {
        str[i++] = (x%10) + '0';
        x = x/10;
    }

    /* If number of digits required is more, then add 0s at the beginning */
    while (i < d)
        str[i++] = '0';

    reverse(str, i);
    str[i] = '\0';
    return i;
}

/* Converts a floating point number to string. */
void ftoa(float n, char *res, int afterpoint)
{
    int next_pos = 0;
    int length;
    /* Check sign */
    if (n<0){
        res[0]='-';
        n = -n;
        next_pos=1;
    }

    /* Extract integer part */
    int ipart = (int)n;

    /* Extract floating part */
    float fpart = n - (float)ipart;

    /* convert integer part to string */
    length = intToStr(ipart, res+next_pos, 1);
    next_pos = next_pos + length;

    /* check for display option after point */
    if (afterpoint != 0)
    {
        res[next_pos] = '.';  /* add dot */
        next_pos++;

       /*  Get the value of fraction part upto given no.
         of points after dot. The third parameter is needed
         to handle cases like 233.007 */
        fpart = fpart * pow(10, afterpoint);
        intToStr((int)fpart, res + next_pos, afterpoint);
    }
}


// Conversi� d'ADC a forces G
float conversio(uint16_t val, float Gtemp,short z)
{
    float resultat=0;

    resultat=((3.3*val)/CONVERSION_INTERVAL);
    if (z==0){
        resultat=((resultat-1.65)/(0.660*(1+0.0001*Gtemp)))+(0.7*0.001);
    }else{
        resultat=((resultat-1.65)/(0.660*(1+0.0004*Gtemp)))+(0.4*0.001);
    }

    return resultat;

}

