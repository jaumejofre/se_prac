/*
 *
 * Jaume Jofre Bravo
 * Sistems Encastats Pr�ctica
 * UOC I semestre 2017-18
 *
 */

#ifndef ADC14_DRIVER_H_
#define ADC14_DRIVER_H_

#define NUM_ADC_CHANNELS 3

#include <stdint.h>
// Includes FreeRTOS
//#include "FreeRTOS.h"
//#include "semphr.h"

volatile uint8_t ADC_reading_available;

extern void init_ADC(void); //uses interrupt ISR ADC14_IRQHandler

extern uint16_t *ADC_read(void);

//// Declaraci� d'un semafor binari per sincronitzar lectures cua
//SemaphoreHandle_t xBinarySemaphoreISR;


#endif /* ADC14_DRIVER_H_ */
