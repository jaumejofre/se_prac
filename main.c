/*
 * main.c
 *
 * Jaume Jofre Bravo
 * Sistems Encastats Pr�ctica
 * UOC I semestre 2017-18
 *
 */

// Includes standard
#include <stdio.h>
#include <stdint.h>
#include <string.h>

// Includes FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

// Includes drivers
#include "i2c_driver.h"
#include "temp_sensor.h"
#include "opt3001.h"
#include "tmp006.h"
#include <adc14_driver.h>
#include <acc_driver.h>

// Inludes LCD
#include <driverlib.h>
#include <grlib.h>
#include "Crystalfontz128x128_ST7735.h"

/* Graphic library context */
Graphics_Context g_sContext;

// Prioritats de les tasques
#define TASK1_PRIORITY    1 //Led - 950 / 50
#define TASK2_PRIORITY    4 //Temp-x1 & Lux-x2 500
#define TASK3_PRIORITY    2 //Accelerometer - 100
#define TASK4_PRIORITY    3 //Display - 100
#define configMINIMAL_STACK_SIZE_Task4  ( ( uint16_t ) 400 )

//Constants
#define QUEUE_LENGTH 10
#define QUEUE_ITEM_SIZE_ACC sizeof(xMessage)
#define QUEUE_ITEM_SIZE_TL sizeof(xSensor)
#define NUM_ADC_CHANNELS 3

// Estructura Accelerometer
struct AMessage
{
    float valX;
    float valY;
    float valZ;
} xMessage;

// Tipus sensor
typedef enum Sensor
{
    elux = 1, etemp = 2
} Sensor;

// Estructura Sensors
struct ASensor
{
    Sensor sensor;
    float value;
} xSensor;

// Recull lectures
uint16_t *Data;

//Conversi� de float a char
char message[10];
char value[5];

//Temperatura global
float GTemp = 20;

// Declaraci� cua
QueueHandle_t xQueueAcc;
QueueHandle_t xQueueTL;

// Prototips de funcions privades
static void prvSetupHardware(void);
static void prvTask1(void *pvParameters);
static void prvTask2(void *pvParameters);
static void prvTask3(void *pvParameters);
static void prvTask4(void *pvParameters);
void drawTitle(void);
void drawDades(float X, float Y, float Z, float lux, float CurTmpAve,
               float OldTmpAve, float dif, int contTask4);

// Declaraci� d'un mutex per acces �nic a var emp Global
SemaphoreHandle_t xMutexGTemp;

// Declaraci� d'un sem�for binari per la ISR d'ADC14
SemaphoreHandle_t xBinarySemaphoreISR;

int main(void)
{
    // Inicializacion de semaforo binario
    xBinarySemaphoreISR = xSemaphoreCreateBinary();
    // Inicializacio de mutex
    xMutexGTemp = xSemaphoreCreateMutex();

    // Comprova si sem�for i mutex s'han creat b�
    if ((xBinarySemaphoreISR != NULL) && (xMutexGTemp != NULL))
    {
        // Inicializaci� del maquinari (clocks, GPIOs, IRQs)
        prvSetupHardware();

        // Creaci� de la cua
        xQueueAcc = xQueueCreate(QUEUE_LENGTH, QUEUE_ITEM_SIZE_ACC);
        xQueueTL = xQueueCreate(QUEUE_LENGTH, QUEUE_ITEM_SIZE_TL);

        // Creaci� de tasca Task1: LED
        xTaskCreate(prvTask1, "Task1", configMINIMAL_STACK_SIZE,
        NULL,
                    TASK1_PRIORITY, NULL);

        // Creaci� de tasca Task2: TEMP & LUX
        xTaskCreate(prvTask2, "Task2", configMINIMAL_STACK_SIZE,
        NULL,
                    TASK2_PRIORITY, NULL);

        // Creaci� de tasca Task3: ACC
        xTaskCreate(prvTask3, "Task3", configMINIMAL_STACK_SIZE,
        NULL,
                    TASK3_PRIORITY, NULL);

        // Creaci� de tasca Task4; DISPLAY
        xTaskCreate(prvTask4, "Task4", configMINIMAL_STACK_SIZE_Task4,
        NULL,
                    TASK4_PRIORITY, NULL);

        vTaskStartScheduler();
    }
    return 0;
}

// Inicializaci� del maquinari del sistema
static void prvSetupHardware(void)
{

    extern void FPU_enableModule(void);

    // Configuraci� del pin P1.0 - LED 1 - com sortida i conf en OFF
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);

    // Inicializaci� de pins sobrants per reducir consum
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PE, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PE, PIN_ALL16);

    // Habilita la FPU
    MAP_FPU_enableModule();
    // Canvia el nombre de "wait states" del controlador de Flash
    MAP_FlashCtl_setWaitState(FLASH_BANK0, 2);
    MAP_FlashCtl_setWaitState(FLASH_BANK1, 2);

    // Selecciona la freq��ncia central d'un rang de freq��ncies del DCO
    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_48);

    // Inicialitza els clocks HSMCLK, SMCLK, MCLK y ACLK
    MAP_CS_initClockSignal(CS_HSMCLK, CS_DCOCLK_SELECT,
    CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT,
    CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT,
    CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_ACLK, CS_REFOCLK_SELECT,
    CS_CLOCK_DIVIDER_1);

    // Selecciona el nivell de tensi� del core
    MAP_PCM_setCoreVoltageLevel(PCM_VCORE0);

    // Inicializaci� del I2C
    I2C_init();
    // Inicializaci� del sensor TMP006
    TMP006_init();
    // Inicializaci� del sensor opt3001
    sensorOpt3001Init();
    sensorOpt3001Enable(true);

    //Inicializa ADC
    init_ADC();

    /* Inicializa LCD */
    Crystalfontz128x128_Init();

    /* Defineix orientaci� per defecte de la pantalla */
    Crystalfontz128x128_SetOrientation(LCD_ORIENTATION_UP);

    /* Inicialitza context gr�fic */
    Graphics_initContext(&g_sContext, &g_sCrystalfontz128x128);
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
    GrContextFontSet(&g_sContext, &g_sFontFixed6x8);
    drawTitle();

    MAP_Interrupt_enableMaster();
}

//Tasca heart beat
static void prvTask1(void *pvParameters)
{
    static const TickType_t xBlinkOn = pdMS_TO_TICKS(10);
    static const TickType_t xBlinkOff = pdMS_TO_TICKS(990);

    for (;;)
    {
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay(xBlinkOn);
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay(xBlinkOff);
    }
}

//Tasca lectura temperatura & llum
static void prvTask2(void *pvParameters)
{
    uint16_t rawData;
    float temperature;
    float convertedLux;
    int contTask2 = 0;
    static const TickType_t xInterval = pdMS_TO_TICKS(500);

    for (;;)
    {
        contTask2++;

        // Llegeix Lux
        sensorOpt3001Read(&rawData);
        sensorOpt3001Convert(rawData, &convertedLux);
        xSensor.sensor = elux;
        xSensor.value = convertedLux;
        //Enviem a cua Lux
        if ((!uxQueueSpacesAvailable(xQueueTL)) == 0)
        {
            xQueueSend(xQueueTL, (void * ) &xSensor, portMAX_DELAY);
        }
        // Llegeix temperatura
        if (contTask2 == 2)
        {
            temperature = TMP006_readAmbientTemperature();
            xSensor.sensor = etemp;
            xSensor.value = temperature;
            //Enviem a cua temperatura
            if ((!uxQueueSpacesAvailable(xQueueTL)) == 0)
            {
                xQueueSend(xQueueTL, (void * ) &xSensor, portMAX_DELAY);
            }
            contTask2 = 0;
        }
        vTaskDelay(xInterval);
    }
}

//Tasca Accelerometer
static void prvTask3(void *pvParameters)
{

    static const TickType_t xMaxReadTime = pdMS_TO_TICKS(100);
    float temp = 20;
    int contTask3 = 0;

    xSemaphoreTake(xBinarySemaphoreISR, 0);

    for (;;)

    {
        //Augmenta comptador per controlar execucions de la tasca
        contTask3++;
        Data = ADC_read();
        if ((!uxQueueSpacesAvailable(xQueueAcc)) == 0)
        {   //Cada 30 segons recupera valor TempGlobal
            if (contTask3 == 300)
            {
                xSemaphoreTake(xMutexGTemp, portMAX_DELAY);
                {
                    temp = GTemp;
                    contTask3 = 0;
                }
                xSemaphoreGive(xMutexGTemp);
            }
            //Passem a conversio el valor a calcular,
            //la tmp global i marquem la coordenada z amb 1
            xMessage.valX = conversio((uint16_t) Data[0], temp, 0);
            xMessage.valY = conversio((uint16_t) Data[1], temp, 0);
            xMessage.valZ = conversio((uint16_t) Data[2], temp, 1);
            xQueueSend(xQueueAcc, (void * ) &xMessage, portMAX_DELAY);
        }
        vTaskDelay(xMaxReadTime);
    }
}

//Tasca display
static void prvTask4(void *pvParameters)
{
    static const TickType_t xDisplayTime = pdMS_TO_TICKS(100);
    float temp = 0, lux = 0, luxAnt = 0;
    float X = 0, Y = 0, Z = 0;
    int flag = 0, contTask4 = 0,contTemp=0;

    //Comptador global
    float CurTmpAve = 0;
    float OldTmpAve = 0;

    for (;;)
    {
        vTaskDelay(xDisplayTime);
        contTask4++;

        //Recull lectures Accelerometer
        //Comprova si hem rebut dades de la cua
        if (uxQueueSpacesAvailable(xQueueAcc) != QUEUE_LENGTH)
        {
            xQueueReceive(xQueueAcc, &(xMessage), portMAX_DELAY);
            //Afegim valor rebut al anterior
            X = (X + xMessage.valX);
            Y = (Y + xMessage.valY);
            Z = (Z + xMessage.valZ);
        }

        //Mitja Temp&Lux
        if (uxQueueSpacesAvailable(xQueueTL) != QUEUE_LENGTH)
        {
            xQueueReceive(xQueueTL, &(xSensor), portMAX_DELAY);
            //Lux
            //Fem mitja solapada amb mitja anterior
            if (xSensor.sensor == elux){
                lux = (luxAnt + xSensor.value)/2;
                luxAnt = lux;
            }
            //Temp
            if (xSensor.sensor == etemp){
                temp = temp + xSensor.value;
                contTemp++;
            }
        }

        //Si el contador val 600, ha passat un minut
        //(cada 100 cicle de tasca4 s�n 1 segon)
        if (contTask4 == 600)
        {
            //Reset screen
            drawTitle();

            //Ja han pasasat els dos primers minuts
            if (flag == 1)
            {
                OldTmpAve = CurTmpAve;
                contTask4 = 0;
            }
            //Hem recollit 60 lectures
            CurTmpAve = temp / contTemp;
            // Envia temperatura a var global
            // Cada 60 segons actualizar� var Temp global
            // Intenta agafar el mutex, bloquejant sin� est� disponible
            xSemaphoreTake(xMutexGTemp, portMAX_DELAY);
            {
                GTemp = CurTmpAve;
            }
            // Allibera el mutex
            xSemaphoreGive(xMutexGTemp);
        }

        //Ja podem copiar temperactura actual a variable glob
        if (contTask4 == 1200)
        {
            //Reset screen
            drawTitle();
            //Canvi de variables
            OldTmpAve = CurTmpAve;
            CurTmpAve = temp / contTemp;
            xSemaphoreTake(xMutexGTemp, portMAX_DELAY);
            {
                GTemp = CurTmpAve;
            }
            // Allibera el mutex
            xSemaphoreGive(xMutexGTemp);
            //Reset variables
            flag = 1;
            contTask4 = 0;
            temp = 0;
        }

        //Dades a LCD cada 10 iteracions de Tasca4
        //aproximadament refresh display cada segon
        if ((contTask4 % 10 == 0) && (contTask4 != 0))
        {
            //Hem acumulat 10 lectures d'Acc i podem fer la mitja dividint per 10
            X=X/10;Y=Y/10;Z=Z/10;

            //Display de dades recollides
            drawDades(X,Y,Z,lux,CurTmpAve,OldTmpAve,(CurTmpAve - OldTmpAve), contTask4/10);

            //Reset mitges del Accelerometer
            X=0;Y=0;Z=0;
        }
    }
}

/*
 * Neteja LCD i dibuixa cap�aleres
 */
void drawTitle()
{
    Graphics_clearDisplay(&g_sContext);
    Graphics_drawStringCentered(&g_sContext, "ACCELEROMETER",
    AUTO_STRING_LENGTH,
                                64, 10, OPAQUE_TEXT);

    Graphics_drawStringCentered(&g_sContext, "SENSORS",
    AUTO_STRING_LENGTH,
                                64, 60, OPAQUE_TEXT);
}

void drawDades(float X, float Y, float Z, float lux, float CurTmpAve,
               float OldTmpAve, float dif, int contTask4)
{
    strcpy(message, "X: ");
    ftoa(X, value, 4);
    strcat(message, value);
    Graphics_drawStringCentered(&g_sContext, (int8_t *) message, 12, 64, 20,
    OPAQUE_TEXT);
    strcpy(message, "Y: ");
    ftoa(Y, value, 4);
    strcat(message, value);
    Graphics_drawStringCentered(&g_sContext, (int8_t *) message, 12, 64, 30,
    OPAQUE_TEXT);
    strcpy(message, "Z: ");
    ftoa(Z, value, 4);
    strcat(message, value);
    Graphics_drawStringCentered(&g_sContext, (int8_t *) message, 12, 64, 40,
    OPAQUE_TEXT);

    strcpy(message, "Llum: ");
    ftoa((lux), value, 4);
    strcat(message, value);
    Graphics_drawStringCentered(&g_sContext, (int8_t *) message, 12, 64, 70,
    OPAQUE_TEXT);

    if (CurTmpAve == 0)
    {
        strcpy(message, "TAct:---");
        Graphics_drawStringCentered(&g_sContext, (int8_t *) message, 12, 64, 80,
        OPAQUE_TEXT);
    }
    else
    {
        strcpy(message, "TAct:");
        ftoa(CurTmpAve, value, 4);
        strcat(message, value);
        Graphics_drawStringCentered(&g_sContext, (int8_t *) message, 12, 64, 80,
        OPAQUE_TEXT);
    }
    if ((OldTmpAve) == 0)
    {
        strcpy(message, "TDif:---");
        Graphics_drawStringCentered(&g_sContext, (int8_t *) message, 12, 64, 90,
        OPAQUE_TEXT);
    }
    else
    {
        strcpy(message, "TDif:");
        ftoa((dif), value, 4);
        strcat(message, value);
        Graphics_drawStringCentered(&g_sContext, (int8_t *) message, 12, 64, 90,
        OPAQUE_TEXT);
    }
    //Per debug
//    strcpy(message, "TAnt: ");
//    ftoa(OldTmpAve, value, 4);
//    strcat(message, value);
//    Graphics_drawStringCentered(&g_sContext, (int8_t *) message, 12, 64, 110,
//    OPAQUE_TEXT);
//
//    strcpy(message, "T4: ");
//    intToStr(contTask4, value, 4);
//    //ftoa(contTask4, value, 4);
//    strcat(message, value);
//    Graphics_drawStringCentered(&g_sContext, (int8_t *) message, 12, 64, 120,
//    OPAQUE_TEXT);
}

