/*
 * acc_driver.h
 *
 * Jaume Jofre Bravo
 * Sistems Encastats Pr�ctica
 * UOC I semestre 2017-18
 *
 */

#ifndef ACC_DRIVER_H_
#define ACC_DRIVER_H_

#include <stdint.h>

#define CONVERSION_INTERVAL 16383

float conversio(uint16_t val, float temp, short z);

void reverse(char *str, int len);

int intToStr(int x, char str[], int d);

void ftoa(float n, char *res, int afterpoint);

#endif /* ACC_DRIVER_H_ */
